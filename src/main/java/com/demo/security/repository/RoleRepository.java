package com.demo.security.repository;

import com.demo.security.domain.Role;
import com.demo.security.domain.RoleName;

public interface RoleRepository {
    Role findByName(RoleName name);
}
