package com.demo.security.repository;

import com.demo.security.domain.User;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Repository
@Transactional
public class UserRepositoryImpl implements UserRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public User findByEmail(String email) {
        String sql = "SELECT e FROM User e WHERE e.email = :email";

        TypedQuery<User> query = entityManager.createQuery(sql, User.class);

        query.setParameter("email", email);

        if (query.getResultList() != null && !query.getResultList().isEmpty()) {
            return query.getResultList().get(0);
        } else {
            return null;
        }
    }

    @Override
    public Boolean existsByEmail(String email) {
        User user = findByEmail(email);

        return user != null;
    }

    @Override
    public void save(User user) {
        if (user.getId() != null) {
            entityManager.merge(user);
        } else {
            entityManager.persist(user);
        }
    }
}
