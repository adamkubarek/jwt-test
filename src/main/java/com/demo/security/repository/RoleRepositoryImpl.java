package com.demo.security.repository;

import com.demo.security.domain.Role;
import com.demo.security.domain.RoleName;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

@Repository
@Transactional
public class RoleRepositoryImpl implements RoleRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Role findByName(RoleName name) {
        String sql = "SELECT e FROM Role e where e.name = :name";

        TypedQuery<Role> query = entityManager.createQuery(sql, Role.class);

        query.setParameter("name", name);

        if (query.getResultList() != null && !query.getResultList().isEmpty()) {
            return query.getResultList().get(0);
        } else {
            return null;
        }
    }
}
