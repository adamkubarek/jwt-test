package com.demo.security.repository;

import com.demo.security.domain.User;

public interface UserRepository {
    User findByEmail(String email);

    Boolean existsByEmail(String email);

    void save(User user);
}
