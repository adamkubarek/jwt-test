package com.demo.security.domain;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
